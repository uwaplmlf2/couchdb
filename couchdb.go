// Couchdb provides a simple interface to the CouchDB HTTP API
package couchdb

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
)

// Map HTTP status codes to errors
var errorTable map[int]error = map[int]error{
	400: errors.New("invalid request"),
	401: errors.New("not authorized"),
	403: errors.New("operation forbidden"),
	404: errors.New("document not found"),
	409: errors.New("document conflict"),
}

var (
	ErrInternal = errors.New("internal server error")
)

func httpError(code int) error {
	if code >= 500 {
		return ErrInternal
	}

	return errorTable[code]
}

type Database struct {
	u      *url.URL
	client *http.Client
}

// Change represents the output of a Database _changes request
type Change struct {
	Seq     json.RawMessage `json:"seq"`
	Id      string          `json:"id"`
	Deleted bool            `json:"deleted"`
	Doc     json.RawMessage `json:"doc"`
}

// ScanSeq decodes the sequence from the Change into the value pointed at
// by v. For CouchDB 1.x, the sequence value is an integer, for 2.x it is
// a JSON string.
func (c Change) ScanSeq(v interface{}) error {
	return json.Unmarshal(c.Seq, v)
}

// SeqString returns the sequence value as a string
func (c Change) SeqString() string {
	var s string
	err := json.Unmarshal(c.Seq, &s)
	if err != nil {
		return string([]byte(c.Seq))
	}
	return s
}

// HasDoc returns true if a document is present in Change
func (c Change) HasDoc() bool {
	return c.Doc == nil
}

// ScanDoc decodes the document from the Change into the value pointed at
// by v.
func (c Change) ScanDoc(v interface{}) error {
	return json.Unmarshal(c.Doc, v)
}

// View is a Database view specification
type View struct {
	ddoc, view string
}

func NewView(name string) View {
	var v View
	f := strings.Split(name, "/")
	if len(f) == 2 {
		v.ddoc, v.view = f[0], f[1]
	}
	return v
}

func (v View) String() string {
	return "_design/" + v.ddoc + "/_view/" + v.view
}

type viewResponse struct {
	Offset    int             `json:"offset"`
	Rows      json.RawMessage `json:"rows"`
	TotalRows int             `json:"total_rows"`
}

type viewRow struct {
	Id    string          `json:"id"`
	Key   json.RawMessage `json:"key"`
	Value json.RawMessage `json:"value"`
	Doc   json.RawMessage `json:"doc"`
}

// Rows represents the returned value from a query
type Rows struct {
	dec *json.Decoder
	row viewRow
	err error
}

func newRows(raw json.RawMessage) (*Rows, error) {
	r := Rows{}
	r.dec = json.NewDecoder(bytes.NewReader([]byte(raw)))
	// Read array open bracket
	_, err := r.dec.Token()
	if err != nil {
		return nil, err
	}
	return &r, nil
}

// Next advances to the next row in the query output and
// returns false when the end is reached.
func (r *Rows) Next() bool {
	state := r.dec.More()
	if state {
		r.err = r.dec.Decode(&r.row)
	}
	return state
}

// ScanKey decodes the key from the current row into the
// value pointed at by v.
func (r *Rows) ScanKey(v interface{}) error {
	if r.err != nil {
		return r.err
	}
	return json.Unmarshal(r.row.Key, v)
}

// ScanValue decodes the value from the current row into the
// value pointed at by v.
func (r *Rows) ScanValue(v interface{}) error {
	if r.err != nil {
		return r.err
	}
	return json.Unmarshal(r.row.Value, v)
}

// ScanDoc decodes the document from the current row into the
// value pointed at by v.
func (r *Rows) ScanDoc(v interface{}) error {
	if r.err != nil {
		return r.err
	}
	return json.Unmarshal(r.row.Doc, v)
}

// Id returns the id of the current row.
func (r *Rows) Id() (string, error) {
	if r.err != nil {
		return "", r.err
	}
	return r.row.Id, nil
}

// NewDatabase returns a new Database with the given url
func NewDatabase(svurl, dbname string) *Database {
	u, err := url.Parse(svurl)
	if err != nil {
		return nil
	}
	u.Path = dbname
	jar, _ := cookiejar.New(nil)

	return &Database{
		u:      u,
		client: &http.Client{Jar: jar},
	}
}

// Session opens a new authenticated session
func (d *Database) Session(creds *url.Userinfo) error {
	u := *d.u
	u.User = nil
	u.Path = "_session"

	var b bytes.Buffer
	enc := json.NewEncoder(&b)
	p, _ := creds.Password()
	enc.Encode(map[string]string{"name": creds.Username(), "password": p})

	req, err := http.NewRequest(http.MethodPost, u.String(), &b)
	if err != nil {
		return err
	}
	req.Header["Content-Type"] = []string{"application/json"}

	resp, err := d.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return err
	}

	// Request was successful and the authentication cookie will be
	// stored in d.client.Jar. We can remove any user/password from
	// the database url.
	d.u.User = nil

	return nil
}

// Info returns some Database metadata, the actual format varies with
// the server version.
func (d *Database) Info() (map[string]interface{}, error) {
	info := make(map[string]interface{})
	req, err := http.NewRequest(http.MethodGet, d.u.String(), nil)
	if err != nil {
		return info, err
	}

	resp, err := d.client.Do(req)
	if err != nil {
		return info, err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return info, err
	}

	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&info)
	return info, err
}

// Version returns the CouchDB server version string
func (d *Database) Version() (string, error) {
	u := *d.u
	// Remove path component
	u.Path = ""
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return "", err
	}

	resp, err := d.client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return "", err
	}

	info := struct {
		Version string
	}{}

	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&info)
	return info.Version, err
}

// Get looks up a document associated with id, decodes the JSON
// and stores it in the value pointed to by doc.
func (d *Database) Get(id string, doc interface{}) error {
	u := *d.u
	u.Path = u.Path + "/" + id
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return err
	}

	resp, err := d.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return err
	}

	return json.NewDecoder(resp.Body).Decode(doc)
}

// GetRev returns the revision tag for a document.
func (d *Database) GetRev(id string) (string, error) {
	u := *d.u
	u.Path = u.Path + "/" + id
	req, err := http.NewRequest(http.MethodHead, u.String(), nil)
	if err != nil {
		return "", err
	}

	resp, err := d.client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return "", err
	}
	return resp.Header.Get("ETag"), nil
}

// GetAttachment downloads a document attachment and writes it to an
// io.Writer. Returns the number of bytes written to wtr.
func (d *Database) GetAttachment(id, name string, dest io.Writer) (int64, error) {
	u := *d.u
	u.Path = u.Path + "/" + id + "/" + name
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return 0, err
	}

	resp, err := d.client.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return 0, err
	}
	return io.Copy(dest, resp.Body)
}

// Put stores or updates the contents of doc in the database using id and
// returns the new revision string. If the document is being updated, doc
// must contain the current revision as a value of the "_rev" attribute.
func (d *Database) Put(id string, doc interface{}) (string, error) {
	var b bytes.Buffer
	enc := json.NewEncoder(&b)
	enc.Encode(doc)

	u := *d.u
	u.Path = u.Path + "/" + id
	req, err := http.NewRequest(http.MethodPut,
		u.String(),
		&b)
	if err != nil {
		return "", err
	}

	resp, err := d.client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return "", err
	}

	info := struct {
		Ok      bool
		Id, Rev string
	}{}

	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&info)
	return info.Rev, err
}

// Delete marks the document associated with id as deleted
func (d *Database) Delete(id, rev string) (string, error) {
	u := *d.u
	u.Path = u.Path + "/" + id
	req, err := http.NewRequest(http.MethodDelete,
		u.String(),
		http.NoBody)
	if err != nil {
		return "", err
	}

	if rev == "" {
		rev, err = d.GetRev(id)
		if err != nil {
			return "", err
		}
	}

	req.Header["If-Match"] = []string{rev}
	resp, err := d.client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return "", err
	}

	info := struct {
		Ok      bool
		Id, Rev string
	}{}

	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&info)
	return info.Rev, err
}

// Query returns the results of a view query as a Rows iterator.
func (d *Database) Query(view View, opts map[string]interface{}) (*Rows, error) {
	// JSON encode all query parameters
	params := url.Values{}
	if opts != nil {
		for k, v := range opts {
			b, err := json.Marshal(v)
			if err != nil {
				return nil, fmt.Errorf("%q=%v: %w", k, v, err)
			}
			params.Add(k, string(b))
		}
	}

	u := *d.u
	u.Path = u.Path + "/" + view.String()
	u.RawQuery = params.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := d.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return nil, err
	}

	var output viewResponse
	err = json.NewDecoder(resp.Body).Decode(&output)
	if err != nil {
		return nil, err
	}

	return newRows(output.Rows)
}

const qlenKey int = 42

// CtxWithQlen returns a new Context which stores the queue length (size) of the
// channel that is returned by the Changes method.
func CtxWithQlen(ctx context.Context, qlen int) context.Context {
	return context.WithValue(ctx, qlenKey, qlen)
}

func qlenFromCtx(ctx context.Context) int {
	qlen, ok := ctx.Value(qlenKey).(int)
	if !ok {
		return 1
	}
	return qlen
}

// Changes returns a channel which supplies the output from a continuous
// _changes feed. Use CtxWithQlen to supply the desired size of the returned
// channel (default is 1).
func (d *Database) Changes(ctx context.Context, params url.Values) (<-chan Change, error) {
	params.Set("feed", "continuous")
	// Heartbeat must be set so our goroutine doesn't block forever
	if params.Get("heartbeat") == "" {
		params.Set("heartbeat", "5000")
	}

	u := *d.u
	u.Path = u.Path + "/_changes"
	u.RawQuery = params.Encode()
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := d.client.Do(req)
	if err != nil {
		return nil, err
	}

	if err = httpError(resp.StatusCode); err != nil {
		resp.Body.Close()
		return nil, err
	}

	var item Change
	ch := make(chan Change, qlenFromCtx(ctx))
	go func() {
		defer resp.Body.Close()
		defer close(ch)
		scanner := bufio.NewScanner(resp.Body)
		for scanner.Scan() {
			select {
			case <-ctx.Done():
				return
			default:
			}
			line := scanner.Bytes()
			if len(line) < 2 {
				// Heartbeat line ...
				continue
			}
			if err := json.Unmarshal(line, &item); err == nil {
				select {
				case ch <- item:
				default:
				}
			}
		}
	}()

	return ch, nil
}
