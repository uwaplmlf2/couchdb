package couchdb

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
)

type Attachment struct {
	ContentType string        `json:"content_type"`
	Length      int           `json:"length"`
	Stub        bool          `json:"stub"`
	Digest      string        `json:"digest"`
	Revpos      int64         `json:"revpos"`
	Filename    string        `json:"-"`
	Content     io.ReadCloser `json:"-"`
}

// PutAttachment adds a new attachment to the document identified by id and rev. If
// rev is an empty string, a new empty document is created unless id already exists.
func (d *Database) PutAttachment(id, rev string, att Attachment) (string, error) {
	u := *d.u
	u.Path = u.Path + "/" + id + "/" + att.Filename
	req, err := http.NewRequest(http.MethodPut,
		u.String(),
		att.Content)
	if err != nil {
		return "", err
	}
	req.Header = map[string][]string{
		"Content-Type":   []string{att.ContentType},
		"Content-Length": []string{strconv.Itoa(att.Length)},
	}
	if rev != "" {
		req.Header["If-Match"] = []string{rev}
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return "", err
	}

	info := struct {
		Ok      bool
		Id, Rev string
	}{}

	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&info)
	return info.Rev, err
}

// GetAttachmentList returns an array of attachments for the specified document.
func (d *Database) GetAttachmentList(id string) ([]Attachment, error) {
	type partial struct {
		Atts map[string]Attachment `json:"_attachments"`
	}
	var doc partial

	u := *d.u
	u.Path = u.Path + "/" + id
	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := d.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if err = httpError(resp.StatusCode); err != nil {
		return nil, err
	}

	err = json.NewDecoder(resp.Body).Decode(doc)
	if err != nil {
		return nil, err
	}

	atts := make([]Attachment, 0, len(doc.Atts))
	for k, v := range doc.Atts {
		v.Filename = k
		atts = append(atts, v)
	}

	return atts, nil
}
