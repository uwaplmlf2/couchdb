package couchdb

import (
	"encoding/json"
	"strings"
	"testing"
)

const RESPONSE = `
{
   "total_rows":7,
   "offset":2,
   "rows":[
      {
         "id":"978-0-596-15589-6",
         "key":"Print",
         "value":272
      },
      {
         "id":"978-0-596-52926-0",
         "key":"Print",
         "value":448
      },
      {
         "id":"978-1-565-92580-9",
         "key":"Print",
         "value":648
      }
   ]
}
`

const CHANGE_1X = `
{
    "changes": [
                {
                    "rev": "2-7051cbe5c8faecd085a3fa619e6e6337"
                }
              ],
    "id": "6478c2ae800dfc387396d14e1fc39626",
    "seq": 42
}
`

const CHANGE_2X = `
{
    "changes": [
                {
                    "rev": "2-7051cbe5c8faecd085a3fa619e6e6337"
                }
              ],
    "id": "6478c2ae800dfc387396d14e1fc39626",
    "seq": "3-g1AAAAG3eJzLYWBg4MhgTmHgz8tPSTV0MDQy1zMAQsMcoARTIkOS_P___7MSGXAqSVIAkkn2IFUZzIkMuUAee5pRqnGiuXkKA2dpXkpqWmZeagpu_Q4g_fGEbEkAqaqH2sIItsXAyMjM2NgUUwdOU_JYgCRDA5ACGjQfn30QlQsgKvcjfGaQZmaUmmZClM8gZhyAmHGfsG0PICrBPmQC22ZqbGRqamyIqSsLAAArcXo"
}
`

func TestQueryResponse(t *testing.T) {
	var resp viewResponse
	if err := json.Unmarshal([]byte(RESPONSE), &resp); err != nil {
		t.Fatal(err)
	}
	rows, err := newRows(resp.Rows)
	if err != nil {
		t.Fatal(err)
	}

	vals := []int64{272, 448, 648}
	count := int(0)
	for rows.Next() {
		var v int64
		err = rows.ScanValue(&v)
		if v != vals[count] {
			t.Errorf("Bad value; expected %v, got %v", vals[count], v)
		}
		count++
	}

	if count != 3 {
		t.Errorf("Bad row count:  %d", count)
	}
}

func TestView(t *testing.T) {
	table := []struct {
		in, out string
	}{
		{in: "status/alldata", out: "_design/status/_view/alldata"},
		{in: "foo", out: "_design//_view/"},
	}

	for _, e := range table {
		v := NewView(e.in)
		if v.String() != e.out {
			t.Errorf("Bad view; expected %q, got %q", e.out, v.String())
		}
	}
}

func TestChange(t *testing.T) {
	var (
		c    Change
		iseq int
		seq  string
	)

	err := json.Unmarshal([]byte(CHANGE_1X), &c)
	if err != nil {
		t.Fatal(err)
	}

	err = c.ScanSeq(&iseq)
	if err != nil {
		t.Fatal(err)
	}
	if iseq != 42 {
		t.Errorf("Bad seq value; expected 42, got %d", iseq)
	}
	seq = c.SeqString()
	if seq != "42" {
		t.Errorf("Bad seq value as string: %q", seq)
	}

	err = json.Unmarshal([]byte(CHANGE_2X), &c)
	if err != nil {
		t.Fatal(err)
	}

	err = c.ScanSeq(&seq)
	if err != nil {
		t.Fatal(err)
	}

	if !strings.HasPrefix(seq, "3-") {
		t.Errorf("Unexpected seq value: %q", seq)
	}

	seq2 := c.SeqString()
	if seq2 != seq {
		t.Errorf("Sequence mismatch; expected %q, got %q", seq, seq2)
	}
}
